#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window/Keyboard.hpp> 
#include "block.h"

std::vector<block*> blocks;
bool isPaused;

void renderingThread(sf::RenderWindow* window) {

    sf::Clock c;
    window->setActive(true);

    //load background texture
    sf::Texture texture;
    if (!texture.loadFromFile("texture.png"))
        std::cout << "failed to load texture";
    sf::Sprite sprite;
    sprite.setTexture(texture);


    while (window->isOpen()) {

        window->draw(sprite);

        //for every block
        for (int i = 0; i < blocks.size(); i++) {
           block* b = blocks.at(i);

           b->renderMe(window); //render the block

           //update location
            if(!isPaused)
                b->nextInstant(c.getElapsedTime());

            //conditions for removing block from sight
            if (b->getY() >= 550) {
                delete b;
                blocks.erase(blocks.begin() + i);
                blocks.push_back(new block());
                isPaused = true;

            }
        }


        window->display();
        c.restart();
    }
}

int main() {
    sf::RenderWindow window(sf::VideoMode(800, 600), "Title");
    window.setActive(false);
    isPaused = true;
    blocks.push_back(new block());
    sf::Event event;

    //start handling rendering (thread 1)
    sf::Thread thread(&renderingThread, &window);
    thread.launch();

    //start handling input (thread 0)
    while (window.isOpen()) {   
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed)
                window.close();

            if(event.type == sf::Event::MouseButtonPressed)
                if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
                {
                    sf::Vector2i mousePos = sf::Mouse::getPosition(window);
                    blocks.at(0)->newPebble(mousePos.x, mousePos.y);
                }
            
            if(event.type == sf::Event::KeyPressed)
                if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
                    isPaused = !isPaused;
                }

        }

    }
}
