#include "block.h"
#include <iostream>
#include <SFML/Graphics.hpp>

//block doesn't have an x and y until we add a pebble
block::block() {
	vel = 0.0f;
}

//add a pebble (blocks are made up of pebbles)
void block::newPebble(float x, float y) {
	if (pebbles.size() == 0) {
		pebbles.push_back(pebble(x, y));
		this->x = x;
		this->y = y;
	}
	else {
		//pebbles have a relative position to first pebble
		pebbles.push_back(pebble(x-this->x, y-this->y));
	}
}

void block::clearPebbles() {
	pebbles.clear();
}

//block's configuration after dt has passed
void block::nextInstant(sf::Time dt) {
	if (y >= 550) {
		y = 550;
		vel = 0;
	}
	else {
		vel += 1.5 * dt.asSeconds();
		y += vel;
	}
}

void block::renderMe(sf::RenderWindow* window) {
	sf::VertexArray quad(sf::Quads, 4);
	int edgeSize = 10;

	if (pebbles.size() > 0) {
		quad[0].position = sf::Vector2f(x + edgeSize, y + edgeSize);
		quad[1].position = sf::Vector2f(x - edgeSize, y + edgeSize);
		quad[2].position = sf::Vector2f(x - edgeSize, y - edgeSize);
		quad[3].position = sf::Vector2f(x + edgeSize, y - edgeSize);

		for (int i = 0; i < 4; i++)
			quad[i].color = sf::Color::Red;

		window->draw(quad);
	}

	for (int i = 1; i < pebbles.size(); i++) {
		pebble* p = &(pebbles.at(i));
		quad[0].position = sf::Vector2f(x + p->x + edgeSize, y + p->y + edgeSize);
		quad[1].position = sf::Vector2f(x + p->x - edgeSize, y + p->y + edgeSize);
		quad[2].position = sf::Vector2f(x + p->x - edgeSize, y + p->y - edgeSize);
		quad[3].position = sf::Vector2f(x + p->x + edgeSize, y + p->y - edgeSize);
		for (int i = 0; i < 4; i++)
			quad[i].color = sf::Color::Red;
		window->draw(quad);
	}
}

float block::getY() {
	return y;
}