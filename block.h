#pragma once
#include <SFML\System\Time.hpp>
#include <SFML/Graphics.hpp>

class pebble
{
public:
	float x;
	float y;
	pebble(float x, float y) { this->x = x; this->y = y; }
};

class block
{
public:
	block();
	void nextInstant(sf::Time dt);
	void renderMe(sf::RenderWindow* window);
	float getY();
	void newPebble(float x, float y);
	void clearPebbles();
private:
	float x;
	float y;
	float vel;
	std::vector<pebble> pebbles;
};


